#!/bin/bash
echo "updating system"
sleep 1
sudo pacman -Syu
sudo pacman -S --needed git base-devel
echo "cloning yay from git repo"
sleep 1
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
echo "yay! snapd"
sleep 3
yay -S snapd
echo "all done"
